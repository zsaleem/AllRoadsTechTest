/*global EventDispatcher:false*/
/*global ChatPanel:false*/
window.onload = function() {
    window.Vents = new EventDispatcher();

    new ChatPanel('User A', '#chat_panel_a');
    new ChatPanel('User B', '#chat_panel_b');
    new ChatPanel('User C', '#chat_panel_c');
};

