;(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.ChatPanel = factory();
    }
}(this, function(b) {

    var ChatPanel = function(user, el) {
        var $chatPanel = $(el);
        var $btnSend = $chatPanel.find('button');

        this.$chatPanel = $chatPanel.find('textarea');
        this.$user = $chatPanel.find('label');
        this.username = user;
        this.$chatList = $('.chat_list');

        this._setUsername();

        $btnSend.on('click', this._onSendMessage.bind(this));
        Vents.listenEvent('updateMessage', this._onUpdateMessage.bind(this));
    };

    ChatPanel.prototype = (function() {

        var _onSendMessage = function(e) {
            var message = this.$chatPanel.val();

            this.$chatPanel.val('');

            Vents.dispatchEvent('updateMessage', {
                user: this.username,
                message: message
            });

            e.preventDefault();
        },

        _onUpdateMessage = function(data) {
            var $p = $('<p />', {
                class: 'user'
            });

            var $user = $('<b />', {
                text: data.user.toLowerCase().replace(' ', '-') + ': '
            });

            $p.append($user);
            $p.append(data.message);

            this.$chatList.append($p);
        },

        _setUsername = function() {
            this.$user.html(this.username);
        };

        return {
            _onSendMessage: _onSendMessage,
            _onUpdateMessage: _onUpdateMessage,
            _setUsername: _setUsername
        };

    })();

    return ChatPanel;
}));


