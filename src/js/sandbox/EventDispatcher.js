;(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        define(factory);
    } else if (typeof exports === 'object') {
        module.exports = factory();
    } else {
        root.EventDispatcher = factory();
    }
}(this, function() {
    console.log('FACT');

    var EventDispatcher = function() {
    };

    EventDispatcher.prototype = (function() {

        var events = {};

        var listenEvent = function(eventName, callback) {
            if (!eventName) return;
            if (!callback || typeof callback !== 'function') return;

            if (!events.hasOwnProperty(eventName)) {
                events[eventName] = callback;
            }
        },

        dispatchEvent = function(eventName, data) {
            if (!eventName) return;

            for (var key in events) {
                if (events.hasOwnProperty(eventName) && key === eventName) {
                    events[eventName](data || {});
                }
            }
        };

        return {
            dispatchEvent: dispatchEvent,
            listenEvent: listenEvent
        };

    })();

    return EventDispatcher;

}));


