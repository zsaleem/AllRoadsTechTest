##Requirements
Below is the stack used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - HTML5.
 - CSS
 - jQuery
 - JSHint.
 - [CSSlint](https://github.com/CSSLint/csslint).
 - [Bower](http://bower.io/).
 - Expressjs.
 - VIM.
 - Git.
 - Gitlab.
 - Mac OS X.
 - Grunt (as build tool) with following plugin!
   - [CSSmin](https://github.com/gruntjs/grunt-contrib-cssmin)
   - [Concat](https://github.com/gruntjs/grunt-contrib-concat)
   - [JSHint](https://github.com/gruntjs/grunt-contrib-jshint)
   - [CSSLint](https://github.com/gruntjs/grunt-contrib-csslint)
   - [Clean](https://github.com/gruntjs/grunt-contrib-clean)
   - [HTMLmin](https://github.com/gruntjs/grunt-contrib-htmlmin)
   - [ProcessHTML](https://github.com/dciccale/grunt-processhtml)
   - [Copy](https://github.com/gruntjs/grunt-contrib-copy)
   - [Nodemon](https://github.com/ChrisWren/grunt-nodemon)
   - [Concurrent](https://github.com/sindresorhus/grunt-concurrent)
   - [Watch](https://github.com/gruntjs/grunt-contrib-watch)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository.
    2. CD to the root folder of this project.
    3. Run `npm install` (if it complains then run `sudo npm install`) to install all Grunt plugins(dependencies). Please make sure that nodejs is installed on your machine before running npm install.
    4. Run `bower install` to install all project dependencies i.e. jquery into js/libs folder. Please make sure that bower is install on your machine before running bower install command.
    5. Now run `grunt` command on your terminal in `root` folder of this project.
    6. Now go to your browser and type `localhost:8000` to view this project in action.

## Description
Above steps, in getting started section, will install all dependencies required for this project to run and make the project ready for
production by minifying all the JavaScript and files. It will place the production ready project in `dist` folder in `root`.

I created a `ChatPanel` module which is located in `src/js/modules/` folder. `ChatPanel` module is responsible of handling users' chat messages to display them in `ChatList` area. Moreover, I also created an event system which is based on `pubsub` design pattern. What happens is when user enter text in textarea then press send `ChatPanel` trigger event using `EventDispatcher` which I created in `src/js/sandbox/` folder. That event is listened in the constructor of `ChatPanel` and update the `ChatList` area in html.

In current example, there are 3 chat panels but you can create as many as you want.

## Usage

In order to use `ChatPanel` module. Simply create html markup in index.html file and give it a unique id. Then in `main.js` file create new instance of `ChatPanel` using `new` operator and pass `username` and first argument and chat panel's html markup's id as second argument such as `new ChatPanel('Zafar', '#chat_panel_d');`.


